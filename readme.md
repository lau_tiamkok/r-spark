# References

https://stackoverflow.com/questions/39494484/sparkr-vs-sparklyr
http://spark.rstudio.com
https://spark.apache.org/docs/1.6.0/sparkr.html
https://www.r-bloggers.com/using-apache-sparkr-to-power-shiny-applications-part-i-2/
https://www.r-bloggers.com/combining-hadoop-spark-r-sparkr-and-shiny-and-it-works/


# Hadoop

## What is a Relational Database?

Traditional RDBMS (relational database management system) have been the de facto standard for database management throughout the age of the internet. The architecture behind RDBMS is such that data is organized in a highly-structured manner, following the relational model. Though, RDBMS is now considered to be a declining database technology. While the precise organization of the data keeps the warehouse very "neat", the need for the data to be well-structured actually becomes a substantial burden at extremely large volumes, resulting in performance declines as size gets bigger. Thus, RDBMS is generally not thought of as a scalable solution to meet the needs of 'big' data.

## What is NoSQL?

NoSQL (commonly referred to as "Not Only SQL") represents a completely different framework of databases that allows for high-performance, agile processing of information at massive scale. In other words, it is a database infrastructure that as been very well-adapted to the heavy demands of big data.

The efficiency of NoSQL can be achieved because unlike relational databases that are highly structured, NoSQL databases are unstructured in nature, trading off stringent consistency requirements for speed and agility. NoSQL centers around the concept of distributed databases, where unstructured data may be stored across multiple processing nodes, and often across multiple servers. This distributed architecture allows NoSQL databases to be horizontally scalable; as data continues to explode, just add more hardware to keep up, with no slowdown in performance. The NoSQL distributed database infrastructure has been the solution to handling some of the biggest data warehouses on the planet – i.e. the likes of Google, Amazon, and the CIA.

## What is Hadoop?

Hadoop is not a type of database, but rather a software ecosystem that allows for massively parallel computing. It is an enabler of certain types NoSQL distributed databases (such as HBase), which can allow for data to be spread across thousands of servers with little reduction in performance.

A staple of the Hadoop ecosystem is MapReduce, a computational model that basically takes intensive data processes and spreads the computation across a potentially endless number of servers (generally referred to as a Hadoop cluster). It has been a game-changer in supporting the enormous processing needs of big data; a large data procedure which might take 20 hours of processing time on a centralized relational database system, may only take 3 minutes when distributed across a large Hadoop cluster of commodity servers, all processing in parallel.

@ref: https://datajobs.com/what-is-hadoop-and-nosql

## What is Hadoop?: SQL Comparison

https://www.youtube.com/watch?v=MfF750YVDxM

## What is Hadoop?

https://www.youtube.com/watch?v=9s-vSeWej1U

## What is the difference between a Hadoop database and a traditional Relational Database?

Well, basically the difference is that Hadoop isn't a database at all.

Hadoop is basically a distributed file system (HDFS) - it lets you store large amount of file data on a cloud of machines, handling data redundancy etc.

On top of that distributed file system, Hadoop provides an API for processing all that stored data - Map-Reduce.
The basic idea is that since the data is stored in many nodes, you're better off processing it in a distributed manner where each node can process the data stored on it rather than spend a lot of time moving it over the network.

Unlike RDMS that you can query in realtime, the map-reduce process takes time and doesnt produce immediate results.

On top of this basic scheme you can build a Column Database, like HBase.
A column-database is basically a hashtable that allows realtime queries on rows.

https://www.quora.com/What-is-the-difference-between-a-Hadoop-database-and-a-traditional-Relational-Database


http://www.aptude.com/blog/entry/hadoop-vs-mongodb-which-platform-is-better-for-handling-big-data
https://github.com/EclairJS/eclairjs-node
http://eclairjs.github.io/

fedexgrp@gdexpress.com
